<?php
namespace App\Core;
class View {
    private $data = array();

    private $render = FALSE;

    public function __construct($template)
    {
        try {
            $file =  '../app/views/' . strtolower($template) . '.php';

            if (file_exists($file)) {
                $this->render = $file;
            } else {
                throw new ViewRenderException('Template ' . $template . ' not found!');
            }
        }
        catch (ViewRenderException $e) {
            echo $e->errorMessage();
        }
    }

    /*
    * Asign variable to view
    * @param mixed $variable
    * @param mixed $value
    */
    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
    }

    public function __destruct()
    {
        extract($this->data);
        include($this->render);

    }
}