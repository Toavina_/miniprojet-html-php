<?php
namespace App\Core;

class Mailer {
    private $transport;
    private $mailer;
    public function __construct(){
        $config = $GLOBALS['config'];
        $this->transport = (new \Swift_SmtpTransport($config['smtp_host'], $config['port']))
            ->setUsername($config['username'])
            ->setPassword($config['password'])
        ;
        $this->mailer = new \Swift_Mailer($this->transport);
    }
    /*
    * Send mail method
    * @param string $to
    * @param string $object
    * @param string $body 
    * @return mixed
    */
    public function sendMail($to,$object,$body){
        $config = $GLOBALS['config'];
        $message = (new \Swift_Message($object))
            ->setFrom([$config['username'] => $config['username']])
            ->setTo([$to, $to => $to])
            ->setBody($body)

        ;

        // Send the message
        return $this->mailer->send($message);
    }
}