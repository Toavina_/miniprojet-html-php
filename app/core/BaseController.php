<?php
namespace App\Core;

class BaseController{
    protected $phpView;
   

    protected function render($template){
        $view = new View($template);
        return $view;
    }
}