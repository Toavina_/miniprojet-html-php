<?php
namespace App\Controller;

use App\Core\Mailer;

class OrderController extends Controller{

    public function show($car){
        foreach($GLOBALS['cars'] as $c){
            if($c->slug == $car){
                $datac = $c;
                break;
            }
        }
        $var = [
            "title" => "Location - ".$car,
            "page" => "location",
            "car" => $datac
        ];
        
        return $this->render('car/show')->assign('var',$var);
    }
    
    public function create(){
        header('Content-Type: application/json');
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $mailToAdmin = new Mailer();
        $mailToClient = new Mailer();
        $bodyAdmin = 
        'Commande de Mr/Mme ' .$data->client_name.
        '</br> Commande: '.$data->car.
        '</br> Total: '.$data->price.
        '</br> Du: '.$data->start_at. ' durée ' .$data->day_rent.
        '</br> Commander le '  .date("Y-m-d H:i:s").
        '</br> Contact client '  .$data->client_email;
        $bodyClient = 
        'Vous avez commander sur locature de Mr/Mme ' .$data->client_name.
        '</br> Commande: '.$data->car.
        '</br> Total: '.$data->price.
        '</br> Du: '.$data->start_at. ' durée ' .$data->day_rent.
        '</br> Commander le '  .date("Y-m-d H:i:s").
        $mailToAdmin->sendMail('contact@locature.com','reservation de '.$data->client_name,$bodyAdmin);
        $mailToClient->sendMail($data->client_email,'reservation su Locature '.$data->client_name,$bodyClient);
        
        echo json_encode([
            "status"=>"success",
            "message"=>"Mail envoyer à l'administrateur"
        ]);
    }  
}