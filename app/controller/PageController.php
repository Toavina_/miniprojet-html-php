<?php
namespace App\Controller;

class PageController extends Controller{

    public function index(){
      $var = [
        "title" => "Locature - Acceuil",
        "page" => "home"
      ];
      return $this->render('home')->assign('var',$var);  
    }

    public function about(){ 
      $var = [
        "title" => "Locature - A propos",
        "page" => "about"
      ];    
      return $this->render('about')->assign('var',$var); 
    }

}