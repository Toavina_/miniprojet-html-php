 
 <div class="footer">
        <div class="container">
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <div class="ft-logo"><img width="140px" src="img/logo.png"></div>
                </div>
            </div>
            <hr class="footer-line">
            <div class="row ">
           
                <div class="col-lg-3 col-md-4 col-sm-6 col-12 ">
                    <div class="footer-widget ">
                        <div class="footer-title">Locature</div>
                        <ul class="list-unstyled">
                            <li><a href="/about">A propos</a></li>
                            <li><a href="#">Locations</a></li>
                            <li><a href="#">Prix</a></li>
                            <li><a href="#">Mention legal</a></li>
                        </ul>
                    </div>
                </div>
               
                <div class="col-lg-3 col-md-4 col-sm-6 col-12 ">
                    <div class="footer-widget ">
                        <div class="footer-title">Contact</div>
                        <ul class="list-unstyled">
                            <li><a href="tel:+261 32 12 232 23">+261 32 12 232 23</a></li>
                            <li><a href="mailto:contact@locature.com">contact@locature.com</a></li>
                            <li>Immeuble ccia Antanimena</li>
                        </ul>
                    </div>
                </div>
               
                <div class="col-lg-3 col-md-4 col-sm-6 col-12 ">
                    <div class="footer-widget ">
                        <div class="footer-title">Reseaux sociaux</div>
                        <ul class="list-unstyled">
                            <li><a href="#">Twitter</a></li>
                            <li><a href="#">Google +</a></li>
                            <li><a href="#">Linked In</a></li>
                            <li><a href="#">Facebook</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-12 col-sm-6 col-12 ">
                    <div class="footer-widget ">
                        <h3 class="footer-title">S'inscrire au newsletter</h3>
                        <form>
                            <div class="newsletter-form">
                                <input class="form-control" placeholder="votre email" type="text">
                                <button class="btn btn-default btn-sm" type="submit">Envoyer</button>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center ">
                    <div class="tiny-footer">
                        <p>Miniprojet html Toavina, Mountazir, Michel, Tanjona</p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>