<?php include('../app/views/templates/header.php')  ?>
<section class="logo-header">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 col-md-12 text-center header-content">
                <img src="img/logo.png" width="200px" class="img-fluid logo" alt="logo">
                <h1>Location de voiture avec Locature</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo voluptatum eveniet iure! Culpa
                    quisquam, laborum et,</p>
                <p>
                    <a class="btn btn-primary rounded" href="#louer">Louer une voiture</a>
                </p>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <br>
    <h4 class="text-center" id="louer">Voitures disponible</h2>
    
        <br>
        <div class="row" id="ads">
            <!-- Category Card -->
            <?php foreach($GLOBALS['cars'] as $car):  ?>
            <div class="col-12 col-md-4">
                <div class="card rounded">
                    <div class="card-image">
                        <span class="card-notify-badge"><?= $car->category  ?></span>
                        <span class="card-notify-year">2018</span>
                        <img class="img-fluid" src="<?= $car->image  ?>" alt="Alternate Text" />
                    </div>
                    <div class="card-image-overlay m-auto">
                        <span class="card-detail-badge"><?= $car->type  ?></span>
                        <span class="card-detail-badge">$<?= $car->price  ?></span>
                        <span class="card-detail-badge"><?= $car->kms  ?> Kms</span>
                    </div>
                    <div class="card-content">
                        <h3 class="text-center" style="margin-top:12px"><?= $car->name  ?></h3>
                    </div>
                    <div class="card-button text-center" style="margin-top:7px">
                        <a href="order/<?= $car->slug  ?>" class="btn btn-primary">Reserver</a>
                    </div>
                </div>
            </div>
            <?php endforeach?>
        </div>
</div>

<?php include('../app/views/templates/footer.php')?>
