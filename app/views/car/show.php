<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if(isset($var['title'])): ?>
    <title><?= $var['title'] ?></title>
    <?php else: ?>
    <title>Locature</title>
    <?php endif ?>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
</head>
<body>

<div class="container">
    <div id="row">
        <div>
            <div class="form-popup" id="myForm">
                <form action="/#" id="form" class="form-container">
                    <input type="hidden" id="car" value="<?= $var['car']->name ?>">
                    <h1><?= $var['title'] ?> - $<span id="price"><?= $var['car']->price ?></span>/Jour</h1>
                    <label for="client_name"><b>Nom</b></label>
                    <input type="text" placeholder="Nom" id="client_name" name="client_name" required>

                    <label for="email"><b>Email</b></label>
                    <input type="text" id="email" placeholder="Enter Email" name="email" required>

                    <label for="datepicker"><b>Debut location</b></label>
                    <input type="text" id="datepicker" required>
                    <h3>Total: <span id="total">$<?= $var['car']->price ?></span></h3>
                    <label for="Prix"><b>Durée (jour)</b></label>
                    <div class="slidecontainer">
                        <input type="range" min="1" max="30" value="1" class="slider" id="myRange" required>
                        <span id="day">1</span>
                    </div>
                    <button type="submit" id="submit_btn" class="btn">Louer</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="/js/jquery.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/swal.js"></script>
<script src="/js/app.js"></script>

</body>
</html>
