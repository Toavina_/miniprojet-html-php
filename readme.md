## Mini projet location de voiture Html ESTI

### Instalation

configuration du serveur mail projet dans ```app/config/mail.php ```

Si le projet ne contient pas le dossier  ``` vendor ```:

    composer install
    composer dump-autoload

demarer un serveur web qui pointe vers le dossier public

    php -S localhost:8080 -t public/

utiliser un serveur mail local pour tester les mail [maildev](https://danfarrelly.nyc/MailDev/) ou [mailcatcher](https://mailcatcher.me/)

### Architecture dossier

    -app (logique de l'application)
    |-config (config mail & application)
    |-controller (controllers)
    |-core (classes qui font les liens entre framework et les lib)
    |-views (vues en php)
    -public (point entrer du serveur)
    -router (routers)
    -vendor (librairies et autoload)

