<?php
require_once '../vendor/autoload.php';
require_once '../app/config/mail.php';
$strJsonFileContents = file_get_contents("cars.json");
$cars = json_decode($strJsonFileContents);
$GLOBALS['cars'] = $cars;
$GLOBALS['config'] = $config;
$router = new \Bramus\Router\Router();
$router->setNamespace('\App\Controller');
require '../router/web.php';
$router->run();
