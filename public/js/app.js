$( function() {
    var dateToday = new Date(); 
    $( "#datepicker" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: 'dd-mm-yy',
        minDate: dateToday,
    });
    let price = $('#price').text()
    let $total = $('#total')
    let $day = $('#day')
    $('#myRange').on('input change',function(e){
        $total.text('$'+price * (this.value))
        $day.text(this.value)
    })
    $('#form').on('submit',function(e){
        e.preventDefault()
        let data = {
            client_name: $('#client_name').val(),
            car: $('#car').val(),
            client_email: $('#email').val(),
            start_at: $('#datepicker').val(),
            day_rent: $('#myRange').val(),
            price: $('#total').text(),
            day_rent: $('#day').text()
        }
        $('#submit_btn').attr("disabled", true);
        $.ajax({
            url: '/api/order',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                $('#form')[0].reset()
                $total.text('$'+price)
                $day.text('1')
                Swal.fire(
                    'Merci de votre commande!',
                    data.message,
                    'success'
                )
                $('#submit_btn').attr("disabled", false);
            },
            data: JSON.stringify(data)
        });    
    })

  } );