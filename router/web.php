<?php
$router->get('/', 'PageController@index');
$router->get('/about', 'PageController@about');
$router->get('/order/{car}', 'OrderController@show');
$router->post('/api/order','OrderController@create');

$router->set404(function() {
    header('HTTP/1.1 404 Not Found');
    echo 'Page not found';
});
